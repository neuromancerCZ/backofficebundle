WebStudio BackOfficeBundle
---

Basic setup derived from symfony doc, but lot of things updated according to:

https://symfonycasts.com/screencast/symfony-bundle

which solved few problems not covered by official doc (composer.json PSR4 mods for example)