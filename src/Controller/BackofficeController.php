<?php

namespace Webstudio\BackofficeBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Webstudio\BackofficeBundle\Service\SetupService;

class BackofficeController extends AbstractController
{
    private $em;
    private $parameterBag;

    public function __construct(EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag)
    {
        $this->em = $entityManager;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @Route("/bof", name="backoffice_index")
     */
    public function index()
    {


        return $this->render('@Backoffice/index.html.twig', [
            'controller_name' => 'BackofficeControllerIndex',
            'params_global' => 'aaa',
            'params_bundle' => 'bbb',
        ]);
    }

    /**
     * @Route("/bof/db", name="backoffice_db")
     */
    public function db(SetupService $setupService)
    {

        (null !== $this->em) ? $rs = "<b>mame db</b>" : $rs = "nemame db";

        return $this->render('@Backoffice/index.html.twig', [
            'controller_name' => 'controller: [BackOfficeControllerDB] DB?: ['.$rs.'] SERVICE->getVersion()? ['.$setupService->getVersion().']',
        ]);
    }
}
