<?php


namespace Webstudio\BackofficeBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('back_office');

        $treeBuilder->getRootNode()
            ->children()
            ->arrayNode('setups')
            ->children()
            ->scalarNode('setup1')->end()
            ->scalarNode('setup2')->end()
            ->end()
            ->end() // setups
            ->arrayNode('setups_outer')
            ->children()
            ->scalarNode('setup1')->end()
            ->scalarNode('setup2')->end()
            ->end()
            ->end() // setups_outer
            ->arrayNode('setups_inner')
            ->children()
            ->scalarNode('setup_inner_1')->end()
            ->scalarNode('setup2')->end()
            ->end()
            ->end() // setups_inner
            ->end()
        ;

        return $treeBuilder;
    }
}